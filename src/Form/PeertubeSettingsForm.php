<?php

namespace Drupal\peertube\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure peertube settings for this site.
 */
class PeertubeSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'peertube_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'peertube.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('peertube.settings');

    $form['peertube_instances'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Peertube instances'),
      '#default_value' => $config->get('peertube_instances'),
      '#description' => $this->t('The URL(s) of your PeerTube instances, one by line. For now, only the last one will be supported due to a core issue. Enter the domain name including scheme, for example: %example, without trailing slash.', ['%example' => 'https://framatube.org']),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if (!empty($form_state->getValue('peertube_instances'))) {
      $peertube_instances = $form_state->getValue('peertube_instances');
      $peertube_provider_urls = explode("\r\n", $peertube_instances);
      foreach ($peertube_provider_urls as $peertube_provider_url) {
        if ($peertube_provider_url !== '' && !UrlHelper::isValid($peertube_provider_url, TRUE)) {
          $form_state->setErrorByName('peertube_instances', $this->t('%url is not a valid URL.', ['%url' => $peertube_provider_url]));
        }
        if ($peertube_provider_url[-1] == '/') {
          $form_state->setErrorByName('peertube_instances', $this->t('%url may not have a trailing slash.', ['%url' => $peertube_provider_url]));
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->configFactory->getEditable('peertube.settings')
      ->set('peertube_instances', $form_state->getValue('peertube_instances'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
