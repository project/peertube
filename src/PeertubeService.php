<?php

namespace Drupal\peertube;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Defines PeertubeService class.
 */
class PeertubeService extends ControllerBase {

  /**
   * The PeerTube configuration.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $peerTubeConfig;

  /**
   * Constructs a PeertubeService object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory service.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->peerTubeConfig = $config_factory->get('peertube.settings');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory')
    );
  }

  /**
   * Add PeerTube providers to the list.
   *
   * @return array
   *   The complete providers list with PeerTube ones.
   */
  public function getPeertubeProviders() {
    $peertube_instances = $this->peerTubeConfig->get('peertube_instances');
    $peertube_provider_urls = explode("\r\n", $peertube_instances);

    $peertube_data = [
      'provider_name' => 'PeerTube',
      'provider_url' => 'https://joinpeertube.org',
      'endpoints' => [],
    ];

    foreach ($peertube_provider_urls as $peertube_provider_url) {
      $peertube_data['endpoints'][] = $this->getPeertubeEndpoint($peertube_provider_url);
    }
    return $peertube_data;
  }

  /**
   * Function to get the Peertube provider endpoint.
   *
   * @param string $provider_url
   *   The provider URL.
   *
   * @return array
   *   The provider endpoint representation.
   */
  public function getPeertubeEndpoint($provider_url) {
    return [
      'schemes' => [
        $provider_url . '/videos/watch/*',
      ],
      'url' => $provider_url . '/services/oembed',
      'discovery' => TRUE,
    ];
  }

}
