# PeerTube

This module allows you to add PeerTube videos as Remote videos
from the media module in drupal core.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/peertube).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/peertube).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module requires the following modules:

- [oEmbed Providers](https://www.drupal.org/project/oembed_providers)


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

1. Enable the peertube module and his dependencies and clear the caches.
2. Visit /admin/config/media/peertube and add the domains of your Peertube instances you want to use.
3. Configure the oEmbed Providers module at /admin/config/media/oembed-providers/custom-providers and allow the Peertube provider.
4. Configure the Remote Video media type at /admin/structure/media/manage/remote_video and allow the Peertube provider.
5. You're done, you should be able to add Peertube videos from the instances you allowed!


## Maintainers

- Philippe Joulot - [phjou](https://www.drupal.org/u/phjou)
